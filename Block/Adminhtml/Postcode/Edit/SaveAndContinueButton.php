<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Block\Adminhtml\Postcode\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveAndContinueButton
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Block\Adminhtml\Postcode\Edit
 */
class SaveAndContinueButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ],
            'sort_order' => 80,
        ];
        return $data;
    }
}
