Magento 2 Example Module
======

Introduction
------------
This module intends to be a solution to the User Story presented as an example exercise.

User Story
----------
The client would like to ensure that all customers placing orders over £300 get a superior level of customer service and
so would like all orders placed online, over the £300 threshold, to be allocated to an account manager. The allocation
needs to be done at the point where the order is created. This should still happen even if the payment event fails. The
allocation is to be done by post code area. A list of postcodes & account manager names is supplied for testing. (One
postcode area can have only one account manager; one account manger can be assigned to multiple postcode areas.)

In the Admin backend the client also requires:
- A menu point to access a grid that will show the postcode area covered and the account manager for that post code.
- The Standard Sales grid should be extended so that there is a new column that shows the account manager linked
to the order (a blank value for orders under the threshold or that don't fall in any valid postcode district). Due to
anticipated requests, this column should be added by rewriting & extending the Core grid files
- The client has no need at this time for a method to import postcode/account manager records, but the initial list
should be imported as part of the module initialisation process.
- Ability to change the threshold at which orders get allocated to an account manager


Description
-----------
Postcode Allocation module allows you to define a set of postcodes
with specifics Account Managers who will assist to your customers
when a given threshold has been overwhelmed.

Installation
------------
You can install this module following one of the next options:

#### By composer installation
Edit your composer.json project file by adding following pieces of
configuration:
```
    "require": {
        …
        "mhidalgo/module-example": "1.1.0"
    },
    "repositories": {
        …
        "mhidalgo": {
            "type": "composer",
            "url": "http://mhidalgo.tk/satis"
        }
    },
    "config": {
        …
        "secure-http": false
    },
```
Then just run “`composer update mhidalgo/module-example`” and finally
run “`php bin/magento --clear-static-content module:enable Mhidalgo_Example`”.

Please request access to this module repository before proceed with
this installation.

#### By direct module installation
Create Mhidalgo/Example folders inside app/code and then place all the
code which was provided with our module inside it.
Finally run “`php bin/magento --clear-static-content module:enable Mhidalgo_Example`”.


Module Setup
------------
Once Allocation module is correctly installed you can setup
threshold amount into Stores -> Configuration -> Sales, then
in General section you will find “Threshold for Account Manager
allocation” where you can set a valid numeric value for threshold.

Managing Postcode Allocation
----------------------------
During installation process a bunch of postcodes will be placed
into the database, but you still can manage them through Magento
Admin, in order to do this you will find “Postcode Allocation”
inside Sales, then the grid will show you the current Postcodes
and Account Managers.

As you can see there is a button for create New Postcode Allocations
and you can use the Action column in order to Edit or Delete
current Postcodes. Every postcode can have only one Account
Manager allocation.

Managing Order grid
-------------------
Every time an order has overwhelmed the Threshold amount and it
is being delivered into an specific postcode which is allocated
to an Account Manager it will be automatically assigned, then you
will be able to see the allocated Account Manager into the Order’s
grid.

REST API Documentation
----------------------
Postcode Allocation module is compatible with Magento 2 REST API,
you can get, create, update and delete Postcode allocations from
the API, in order to do this please follow our online documentation
available on this link:

https://documenter.getpostman.com/view/1035286/S1LsYqU1

Troubleshooting
---------------
*Unique constraint violation found*: During Postcode Allocation
creation process you can receive this message which means that you
are trying to create a postcode which already exists, this is not
allowed by our module.
