<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Ui\Component\Form\PostcodeAllocation;

use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Ui\Component\Form\PostcodeAllocation
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\Collection
     */
    protected $collection;

    /**
     * @var \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool
     */
    protected $filterPool;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\Collection $collection
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool $filterPool
     * @param array $meta
     * @param array $data
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\Collection $collection,
        \Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool $filterPool,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collection;
        $this->filterPool = $filterPool;
    }

    /**
     * @return array
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function getData()
    {
        if (!$this->loadedData) {
            $items = $this->collection->getItems();
            foreach ($items as $item) {
                $this->loadedData[$item->getId()] = $item->getData();
            }
        }
        return $this->loadedData;
    }
    
}
