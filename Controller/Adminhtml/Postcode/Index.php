<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Controller\Adminhtml\Postcode;

use Magento\Backend\App\Action;

/**
 * Class Index
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Controller\Adminhtml\Postcode
 */
class Index extends Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mhidalgo_Example::postcode_allocation');
        $resultPage->addBreadcrumb(__('Example'), __('Example'));
        $resultPage->addBreadcrumb(__('Post code Allocation'), __('Post code Allocation'));
        $resultPage->getConfig()->getTitle()->prepend(__('Post code Allocation'));

        return $resultPage;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mhidalgo_Example::postcode_allocation');
    }
}
