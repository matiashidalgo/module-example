<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Controller\Adminhtml\Postcode;

use Magento\Backend\App\Action;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;

/**
 * Class Delete
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Controller\Adminhtml\Postcode
 */
class Delete extends Action
{
    protected $model;
    /**
     * @var Delete \Mhidalgo\Example\Model\PostCodeAllocationRepository
     */
    private $postCodeAllocationRepository;

    /**
     * @param Action\Context                                              $context
     * @param \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface $postCodeAllocRepo
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        Action\Context $context,
        PostCodeAllocationRepositoryInterface $postCodeAllocRepo
    ) {
        parent::__construct($context);
        $this->postCodeAllocationRepository = $postCodeAllocRepo;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mhidalgo_Example::postcode_allocation');
    }

    /**
     * Delete action
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $modelId = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($modelId) {
            try {
                $this->postCodeAllocationRepository->deleteById($modelId);
                $this->messageManager->addSuccessMessage(__('Post Code Allocation deleted'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $modelId]);
            }
        }
        $this->messageManager->addErrorMessage(__('Post Code Allocation does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}
