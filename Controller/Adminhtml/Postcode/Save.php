<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Controller\Adminhtml\Postcode;

use Magento\Backend\App\Action;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;
use Mhidalgo\Example\Model\PostCodeAllocationFactory;

/**
 * Class Save
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Controller\Adminhtml\Postcode
 */
class Save extends Action
{

    /**
     * @var \Mhidalgo\Example\Model\PostCodeAllocationFactory
     */
    private $postCodeAllocationFactory;

    /**
     * @var \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface
     */
    private $postCodeAllocationRepository;

    /**
     * @param Action\Context                                              $context
     * @param \Mhidalgo\Example\Model\PostCodeAllocationFactory           $postCodeAllocFactory
     * @param \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface $postCodeAllocRepo
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        Action\Context $context,
        PostCodeAllocationFactory $postCodeAllocFactory,
        PostCodeAllocationRepositoryInterface $postCodeAllocRepo
    ) {
        parent::__construct($context);
        $this->postCodeAllocationFactory = $postCodeAllocFactory;
        $this->postCodeAllocationRepository = $postCodeAllocRepo;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mhidalgo_Example::postcode_allocation');
    }

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Mhidalgo\Example\Model\PostCodeAllocation $model */
            $model = $this->postCodeAllocationFactory->create();

            $modelId = $this->getRequest()->getParam('id');
            if ($modelId) {
                $model = $this->postCodeAllocationRepository->get($modelId);
            }

            $model->setData($data);

            try {
                $this->postCodeAllocationRepository->save($model);
                $this->messageManager->addSuccessMessage(__('Post Code Allocation saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong while saving the Post Code Allocation')
                );
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
