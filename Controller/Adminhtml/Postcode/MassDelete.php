<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Controller\Adminhtml\Postcode;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;
use Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\CollectionFactory;

/**
 * Class MassDelete
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Controller\Adminhtml\Postcode
 */
class MassDelete extends \Magento\Backend\App\Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface
     */
    private $postCodeAllocationRepository;

    /**
     * @param Context                                                     $context
     * @param Filter                                                      $filter
     * @param CollectionFactory                                           $collectionFactory
     * @param \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface $postCodeAllocRepo
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        PostCodeAllocationRepositoryInterface $postCodeAllocRepo
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->postCodeAllocationRepository = $postCodeAllocRepo;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mhidalgo_Example::postcode_allocation');
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $model) {
            $this->deleteAllocation($model);
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('example/postcode/index');
    }

    /**
     * @param $model
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    private function deleteAllocation($model)
    {
        $this->postCodeAllocationRepository->delete($model);
    }
}
