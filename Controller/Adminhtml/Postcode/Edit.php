<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Controller\Adminhtml\Postcode;

use Magento\Backend\App\Action;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;
use Mhidalgo\Example\Model\PostCodeAllocationFactory;

/**
 * Class Edit
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Controller\Adminhtml\Postcode
 */
class Edit extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Mhidalgo\Example\Model\PostCodeAllocationRepository
     */
    private $postCodeAllocationRepository;

    /**
     * @var \Mhidalgo\Example\Model\PostCodeAllocationFactory
     */
    private $postCodeAllocationFactory;

    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context                         $context
     * @param \Magento\Framework\View\Result\PageFactory                  $resultPageFactory
     * @param \Magento\Framework\Registry                                 $registry
     * @param \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface $postCodeAllocRepo
     * @param \Mhidalgo\Example\Model\PostCodeAllocationFactory           $postCodeAllocFactory
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        PostCodeAllocationRepositoryInterface $postCodeAllocRepo,
        PostCodeAllocationFactory $postCodeAllocFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->postCodeAllocationRepository = $postCodeAllocRepo;
        $this->postCodeAllocationFactory = $postCodeAllocFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mhidalgo_Example::postcode_allocation');
    }

    /**
     * Init actions
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mhidalgo_Example::postcode_allocation')
            ->addBreadcrumb(__('Post Code Allocation'), __('Post Code Allocation'))
            ->addBreadcrumb(__('Edit Post Code Allocation'), __('Edit Post Code Allocation'));
        return $resultPage;
    }

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $modelId = $this->getRequest()->getParam('id');
        $model = $this->postCodeAllocationFactory->create();

        if ($modelId) {
            $model = $this->postCodeAllocationRepository->get($modelId);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Post Code Allocation not exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->coreRegistry->register('current_model', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $modelId ? __('Edit Post Code Allocation') : __('New Post Code Allocation'),
            $modelId ? __('Edit Post Code Allocation') : __('New Post Code Allocation')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Post Code Allocation'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ?
                __('Edit Post Code Allocation: ') . $model->getPostcode() : __('New Post Code Allocation'));

        return $resultPage;
    }
}
