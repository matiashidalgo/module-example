<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Helper;

/**
 * Class Data
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_THRESHOLD = 'sales/general/threshold_account_manager_allocation';

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return mixed
     */
    public function getThreshold()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_THRESHOLD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
