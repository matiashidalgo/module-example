<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Setup;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Mhidalgo\Example\Model\PostCodeAllocationFactory;
use Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation;

/**
 * Class InstallData
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Setup
 */
class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    const COLUMN_POSTCODE = 0;
    const COLUMN_ACCOUNT_MANAGER = 1;

    /**
     * @var InstallData \Magento\Framework\File\Csv
     */
    private $csv;
    /**
     * @var InstallData \Magento\Framework\Module\Dir\Reader
     */
    private $reader;
    /**
     * @var InstallData \Mhidalgo\Example\Model\PostCodeAllocationFactory
     */
    private $postCodeAllocFactory;
    /**
     * @var InstallData \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation
     */
    private $postCodeAllocRes;

    /**
     * InstallData constructor.
     *
     * @param \Magento\Framework\File\Csv                            $csv
     * @param \Magento\Framework\Module\Dir\Reader                   $reader
     * @param \Mhidalgo\Example\Model\PostCodeAllocationFactory        $postCodeAllocFactory
     * @param \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation $postCodeAllocRes
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        Csv $csv,
        Reader $reader,
        PostCodeAllocationFactory $postCodeAllocFactory,
        PostCodeAllocation $postCodeAllocRes
    ) {
        $this->csv = $csv;
        $this->reader = $reader;
        $this->postCodeAllocFactory = $postCodeAllocFactory;
        $this->postCodeAllocRes = $postCodeAllocRes;
    }

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface   $context
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @throws \Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $accountManagersCsv = $this->getModuleDir() . '/Data/account_managers.csv';

        $accountManagers = $this->csv->getData($accountManagersCsv);

        /**
         * Drop csv header
         */
        $accountManagers = array_slice($accountManagers, 1);

        foreach ($accountManagers as $accountManager) {
            $this->importAccountManager($accountManager);
        }

        $setup->endSetup();
    }

    /**
     * @param $accountManagerData
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function importAccountManager($accountManagerData)
    {
        /** @var \Mhidalgo\Example\Model\PostCodeAllocation $postCodeAlloc */
        $postCodeAlloc = $this->postCodeAllocFactory->create();
        $postCodeAlloc->setPostcode($accountManagerData[self::COLUMN_POSTCODE])
            ->setAccountManager($accountManagerData[self::COLUMN_ACCOUNT_MANAGER]);

        $this->postCodeAllocRes->save($postCodeAlloc);
    }

    /**
     * Return full path to a given Module folder
     * @param $type
     * @param $moduleName
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return string
     */
    private function getModuleDir($type = '', $moduleName = 'Mhidalgo_Example')
    {
        return $this->reader->getModuleDir($type, $moduleName);
    }
}
