<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;
use Mhidalgo\Example\Helper\Data;
use Magento\Sales\Model\Order;

/**
 * Class OrderPlaceBefore
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Observer\Sales
 */
class OrderPlaceBefore implements ObserverInterface
{
    /**
     * @var \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface
     */
    private $postCodeAllocationRepository;
    /**
     * @var \Mhidalgo\Example\Helper\Data
     */
    private $helperData;

    /**
     * OrderPlaceAfter constructor.
     *
     * @param \Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface $postCodeAllocRepo
     * @param \Mhidalgo\Example\Helper\Data                               $helperData
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        PostCodeAllocationRepositoryInterface $postCodeAllocRepo,
        Data $helperData
    ) {
        $this->postCodeAllocationRepository = $postCodeAllocRepo;
        $this->helperData = $helperData;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /**
         * @var $order \Magento\Sales\Model\Order
         */
        $order = $observer->getEvent()->getOrder();

        if ($order->getGrandTotal() < $this->helperData->getThreshold()) {
            return;
        }

        $postCode = substr($this->getPostcode($order), 0, -2);

        try {
            $postCodeAllocation = $this->postCodeAllocationRepository->getByPostcode($postCode);

            $order->setAccountManager($postCodeAllocation->getAccountManager());
        } catch (NoSuchEntityException $e) {
            /**
             * No post code allocation found.
             */
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return string
     */
    private function getPostcode(Order $order)
    {
        if ($order->getShippingAddress() && $order->getShippingAddress()->getPostcode()) {
            return $order->getShippingAddress()->getPostcode();
        }
        return $order->getBillingAddress()->getPostcode();
    }
}
