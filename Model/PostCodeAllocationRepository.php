<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Mhidalgo\Example\Api\Data\PostCodeAllocationSearchResultsInterfaceFactory;
use Mhidalgo\Example\Api\PostCodeAllocationRepositoryInterface;
use Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\CollectionFactory;
use Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation as PostCodeAllocationResource;
use Mhidalgo\Example\Api\Data\PostCodeAllocationInterface;

/**
 * Class PostCodeAllocationRepository
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Model
 */
class PostCodeAllocationRepository implements PostCodeAllocationRepositoryInterface
{
    /**
     * @var PostCodeAllocationRepository \Mhidalgo\Example\Model\PostCodeAllocationFactory
     */
    private $postCodeAllocFact;

    /**
     * @var PostCodeAllocationRepository \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PostCodeAllocationRepository \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var PostCodeAllocationRepository \Mhidalgo\Example\Model\PostCodeAllocationSearchResultsInterfaceFactory
     */
    private $postCodeAllocSearchResultsFactory;

    /**
     * @var PostCodeAllocationRepository \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation
     */
    private $postCodeAllocRes;

    /**
     * @var \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface[]
     */
    private $postCodeAllocById = [];

    /**
     * @var \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface[]
     */
    private $postCodeAllocByPostCode = [];

    /**
     * PostCodeAllocationRepository constructor.
     *
     * @param \Mhidalgo\Example\Model\PostCodeAllocationFactory                          $postCodeAllocFact
     * @param \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface       $collectionProcessor
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationSearchResultsInterfaceFactory $postCodeAllocSRFac
     * @param \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation                   $postCodeAllocRes
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    public function __construct(
        PostCodeAllocationFactory $postCodeAllocFact,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        PostCodeAllocationSearchResultsInterfaceFactory $postCodeAllocSRFac,
        PostCodeAllocationResource $postCodeAllocRes
    ) {
        $this->postCodeAllocFact = $postCodeAllocFact;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->postCodeAllocSearchResultsFactory = $postCodeAllocSRFac;
        $this->postCodeAllocRes = $postCodeAllocRes;
    }

    /**
     * @param int $entityId
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($entityId)
    {
        if (isset($this->postCodeAllocById[$entityId])) {
            return $this->postCodeAllocById[$entityId];
        }
        $postCodeAlloc = $this->postCodeAllocFact->create()->load($entityId);
        if (!$postCodeAlloc->getEntityId()) {
            throw NoSuchEntityException::singleField('entity_id', $entityId);
        }
        $this->postCodeAllocById[$entityId] = $postCodeAlloc;
        $this->postCodeAllocByPostCode[$postCodeAlloc->getPostcode()] = $postCodeAlloc;

        return $postCodeAlloc;
    }

    /**
     * @param string $postCode
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByPostcode($postCode)
    {
        if (isset($this->postCodeAllocByPostCode[$postCode])) {
            return $this->postCodeAllocByPostCode[$postCode];
        }
        $postCodeAlloc = $this->postCodeAllocFact->create()->load($postCode, 'postcode');
        if (!$postCodeAlloc->getEntityId()) {
            throw NoSuchEntityException::singleField('postcode', $postCode);
        }
        $this->postCodeAllocById[$postCodeAlloc->getEntityId()] = $postCodeAlloc;
        $this->postCodeAllocByPostCode[$postCode] = $postCodeAlloc;

        return $postCodeAlloc;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var \Mhidalgo\Example\Api\Data\PostCodeAllocationSearchResultsInterface $searchResults */
        $searchResults = $this->postCodeAllocSearchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface $postCodeAllocation
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(PostCodeAllocationInterface $postCodeAllocation)
    {
        try {
            $this->postCodeAllocRes->save($postCodeAllocation);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $postCodeAllocation;
    }

    /**
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface $postCodeAllocation
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(PostCodeAllocationInterface $postCodeAllocation)
    {
        try {
            $this->postCodeAllocRes->delete($postCodeAllocation);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $entityId
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteById($entityId)
    {
        return $this->delete($this->get($entityId));
    }
}
