<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Model;

/**
 * Class PostCodeAllocation
 * @method \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation getResource()
 * @method \Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation\Collection getCollection()
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Model
 */
class PostCodeAllocation extends \Magento\Framework\Model\AbstractModel implements
    \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'mhidalgo_example_postcodeallocation';
    protected $_cacheTag = 'mhidalgo_example_postcodeallocation';
    protected $_eventPrefix = 'mhidalgo_example_postcodeallocation';

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    protected function _construct()
    {
        $this->_init('Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation');
    }

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->getData('postcode');
    }

    /**
     * @param $postCode
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return $this
     */
    public function setPostcode($postCode)
    {
        $this->setData('postcode', $postCode);
        return $this;
    }

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return mixed
     */
    public function getAccountManager()
    {
        return $this->getData('account_manager');
    }

    /**
     * @param $accountManager
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return $this
     */
    public function setAccountManager($accountManager)
    {
        $this->setData('account_manager', $accountManager);
        return $this;
    }
}
