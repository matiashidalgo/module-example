<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation;

/**
 * Class Collection
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     */
    protected function _construct()
    {
        $this->_init(
            'Mhidalgo\Example\Model\PostCodeAllocation',
            'Mhidalgo\Example\Model\ResourceModel\PostCodeAllocation'
        );
    }
}
