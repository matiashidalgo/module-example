<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Model\ResourceModel;

/**
 * Class PostCodeAllocation
 *
 * @author  Matias Hidalgo <me@mhidalgo.tk>
 * @package Mhidalgo\Example\Model\ResourceModel
 */
class PostCodeAllocation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @author Matias Hidalgo <matias.hidalgo@redboxdigital.com>
     */
    protected function _construct()
    {
        $this->_init('example_postcode_allocation', 'entity_id');
    }
}
