<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Api;

use Mhidalgo\Example\Api\Data\PostCodeAllocationInterface;

/**
 * Interface PostCodeAllocationRepositoryInterface
 *
 * @package Mhidalgo\Example\Api
 */
interface PostCodeAllocationRepositoryInterface
{
    /**
     * @param int $entityId
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($entityId);

    /**
     * @param string $postCode
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByPostcode($postCode);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface $postCodeAllocation
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface|\Mhidalgo\Example\Model\PostCodeAllocation
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(PostCodeAllocationInterface $postCodeAllocation);

    /**
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface $postCodeAllocation
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(PostCodeAllocationInterface $postCodeAllocation);

    /**
     * @param int $entityId
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteById($entityId);
}
