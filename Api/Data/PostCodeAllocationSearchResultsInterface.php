<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PostCodeAllocationSearchResultsInterface
 *
 * @package Mhidalgo\Example\Api\Data
 */
interface PostCodeAllocationSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface[]
     */
    public function getItems();

    /**
     * @param \Mhidalgo\Example\Api\Data\PostCodeAllocationInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
