<?php
/**
 * @category  Example
 * @package   Mhidalgo_Example
 * @author    Matias Hidalgo <me@mhidalgo.tk>
 */
namespace Mhidalgo\Example\Api\Data;

/**
 * Interface PostCodeAllocationInterface
 *
 * @package Mhidalgo\Example\Api\Data
 */
interface PostCodeAllocationInterface
{

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return mixed
     */
    public function getPostcode();

    /**
     * @param $postCode
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return $this
     */
    public function setPostcode($postCode);

    /**
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return mixed
     */
    public function getAccountManager();

    /**
     * @param $accountManager
     *
     * @author Matias Hidalgo <me@mhidalgo.tk>
     * @return $this
     */
    public function setAccountManager($accountManager);
}
